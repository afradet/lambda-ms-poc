package lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import io.agroal.api.AgroalDataSource;
import org.flywaydb.core.Flyway;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.sql.Connection;
import java.sql.SQLException;

@Named("main")
@ApplicationScoped
public class MainLambda implements RequestHandler<InputObject, String> {

//    @Inject
//    AgroalDataSource sqliteDb;
    @Inject
    Flyway flyway;

    private static Connection c = null;

    static {
        try {
            c = SqliteDb.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String handleRequest(InputObject input, Context context) {
        System.out.println("connect ok : " + (c != null));
        try {
            System.out.println("Migrating");
            flyway.migrate();
            System.out.println("Migration ok");
//            sqliteDb.getConnection().createStatement().execute("select 1");
            c.createStatement().execute("select 1");
            return "OK";
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "KO";
    }


//
//    /**
//     * @return
//     * @throws SQLException
//     */
//    public Connection getDb() throws SQLException {
//        if (c == null) {
//            c = sqliteDb.getConnection();
//        }
//        return c;
//    }
}
