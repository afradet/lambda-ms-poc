package lambda;

import java.nio.file.FileSystems;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;

import static lambda.S3Mock.DB_FILE;

public class SqliteDb {

    public static Connection getConnection() {

        String tmpDir = System.getProperty("java.io.tmpdir");
        System.out.println("temp dir : " + tmpDir);
        String separator = FileSystems.getDefault().getSeparator();
        String url = "jdbc:sqlite:/" + tmpDir + separator + DB_FILE;

        try {
            Connection conn = DriverManager.getConnection(url);
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("The driver name is " + meta.getDriverName());
                System.out.println("A new database has been created.");
            }
            return conn;

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
}
