package lambda;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class H2Db {

    static String url = "jdbc:h2:~/test;CIPHER=AES";
    static String user = "sa";
    static String pwds = "filepwd userpwd";

    public static Connection getConnection() {
        try {
            var conn = DriverManager.getConnection(url, user, pwds);
            return conn;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
