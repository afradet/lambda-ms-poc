package lambda;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class S3Mock {

    public static final String DIR_PATH = "/tmp/data/";
    public static final String DB_FILE = "sqlite_db";
    public static final Path DB_PATH = Path.of(DIR_PATH + DB_FILE);


    public static File getDbFile() throws IOException {
        if (!Files.exists(DB_PATH)) {
            Files.createFile(DB_PATH);
        }
        return DB_PATH.toFile();
    }

    public static void persistFile() {

    }

}
